#7. Реализовать генератор с помощью функции с ключевым словом yield, создающим очередное значение.
#При вызове функции должен создаваться объект-генератор. Функция должна вызываться следующим образом: for el in fact(n).
#Функция отвечает за получение факториала числа, а в цикле необходимо выводить только первые n чисел, начиная с 1! и до n!.
#Подсказка: факториал числа n — произведение чисел от 1 до n. Например, факториал четырёх 4! = 1 * 2 * 3 * 4 = 24.

from itertools import count
from math import factorial

def fact(number: int):
    for i in count(1):
        if i <= number:
            yield factorial(i)
        else:
            break

n=int(input('Введите по какое число считать факториал: '))
factorials = []
for el in fact(n):
    factorials.append(el)
print(f'\nФакториал: {factorials}')