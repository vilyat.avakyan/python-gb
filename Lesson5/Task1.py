#Создать программно файл в текстовом формате, записать в него построчно данные, вводимые пользователем. Об окончании ввода данных свидетельствует пустая строка.


file = 'task1.txt'
with open(file, mode='a', encoding='utf-8') as file:
    while True:
         user_input = input('Введите строку: ')
         if not user_input:
                file.close()
                break
         file.write(user_input + '\n')
