"""
    Продолжить работу над первым заданием. Разработать методы, отвечающие за приём оргтехники на склад и передачу
    в определенное подразделение компании. Для хранения данных о наименовании и количестве единиц оргтехники,
    а также других данных, можно использовать любую подходящую структуру, например словарь.
"""


class Storage:
    def __init__(self):
        self.__storage = []

    def __str__(self):
        return f"На складе {len(self.__storage)} единиц оргтехники"

    def __getitem__(self, index):
        return self.__storage[index]

    def add(self, item: 'OfficeEquipment'):
        self.__storage.append(item)

    def transfer(self, index: int, department: str):
        item: OfficeEquipment = self.__storage[index]
        item.department = department


class OfficeEquipment:

    def __init__(self, vendor: str, category: str, model: str, color: str, price: float):
        self.vendor = vendor
        self.category = category
        self.model = model
        self.color = color
        self.price = price
        self.departament = None

    def __str__(self):
        return {
            'vendor': self.vendor,
            'category': self.category,
            'model': self.model,
            'color': self.color,
            'price': self.price,
        }

    def __getattribute__(self, name):
        return object.__getattribute__(self, name)


class Printer(OfficeEquipment):
    def __init__(self, is_wireless: bool, *args):
        super().__init__("Printer", *args)
        self.is_wireless = is_wireless


class Scanner(OfficeEquipment):
    def __init__(self, *args):
        super().__init__("Scanner", *args)


class Xerox(OfficeEquipment):
    def __init__(self, is_colorful: bool, *args):
        super().__init__("Xerox", *args)
        self.is_colorful = is_colorful
