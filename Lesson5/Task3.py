file = 'task3.txt'
words = {}
sum = 0

with open(file, mode='r', encoding='utf-8') as file:
    lines = file.readlines()

for i, line in enumerate(lines):
    name, salary = line.split(' ')
    if float(salary) <= 20000:
        print(f'{name} с зарплатой {salary}')
    sum += float(salary)

print(f'Средняя зп: {sum / len(lines)}')
