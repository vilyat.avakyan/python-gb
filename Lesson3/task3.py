#3. Реализовать функцию my_func(), которая принимает три позиционных аргумента, и возвращает сумму наибольших двух аргументов.
arg_1 = float(input('Введите аргумент 1:'))
arg_2 = float(input('Введите аргумент 2:'))
arg_3 = float(input('Введите аргумент 3:'))

def my_func(arg_1, arg_2, arg_3):
    return print(arg_1 + arg_2 + arg_3 - min(arg_1, arg_2, arg_3))
my_func(arg_1, arg_2, arg_3)