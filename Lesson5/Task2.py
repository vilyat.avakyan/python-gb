#Создать текстовый файл (не программно), сохранить в нем несколько строк, выполнить подсчет количества строк, количества слов в каждой строке.

file = 'task2.txt'
words = {}

with open(file, mode='r', encoding='utf-8') as file:
    lines = file.readlines()
for i, line in enumerate(lines):
     words[i + 1] = len(line.split(' '))

print(f'Количество строк: {len(words)}')

for stroka, slova in words.items():
    print(f'В {stroka} строке {slova} слов')
