import re
file_name = 'task6.txt'
with open(file_name, mode='r', encoding='utf-8') as file:
    lines = file.readlines()

subject_dict = {}
for line in lines:
    subject, amount_of_subjects = line.split(': ')
    line_of_hours = re.sub(r'\D', ' ', amount_of_subjects)
    ttl_hours = 0
    for hour in line_of_hours.split():
        ttl_hours += float(hour)
    subject_dict[subject] = ttl_hours

print('Словарь: ')
for key, val in subject_dict.items():
    print(f'{key} - {val}')

