#5. Реализовать формирование списка, используя функцию range() и возможности генератора.
#В список должны войти четные числа от 100 до 1000 (включая границы). Необходимо получить результат вычисления произведения всех элементов списка.
#Подсказка: использовать функцию reduce().
#подсмотрел на http://espressocode.top/python-multiply-numbers-list-3-different-ways/

from functools import reduce

list = [el for el in range(100, 1001) if el % 2 == 0]
result = reduce((lambda x, y: x * y), list)
print(result)